#
# This file is the libps6000 recipe.
#

SUMMARY = "Simple libps6000 application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "https://labs.picotech.com/debian/pool/main/libp/${BPN}/${BPN}_${PV}_armhf.deb"
SRC_URI[md5sum] = "efd1945a8965629d48f1f68b725641a9"

S = "${WORKDIR}"

RDEPENDS_${PN} = "bash libusb1 libpicoipp"

INSANE_SKIP_${PN} = "ldflags"

do_unpack() {
	    ar x ${DL_DIR}/${PN}_${PV}_armhf.deb
}

do_install() {
	     tar -xvf data.tar.xz --directory ${D}
}

FILES_${PN}="/opt/picoscope/lib/*.so.* /opt/picoscope/share/*"
FILES_${PN}-dev="/opt/picoscope/include/* /opt/picoscope/lib/*.so"