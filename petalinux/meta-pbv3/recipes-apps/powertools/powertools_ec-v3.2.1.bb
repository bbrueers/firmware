#
# This file is the powertools recipe.
#

SUMMARY = "ITk Strips Powerboard Control and Test Suite"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "gitsm://gitlab.cern.ch/bbrueers/powertools.git;protocol=https;branch=testAllAtOnce2 \
file://equip_testbench.json"
SRCREV = "${PV}"

S = "${WORKDIR}/git"

DEPENDS = " \
	nlohmann-json \
	json-schema-validator \
"

inherit cmake

EXTRA_OECMAKE += "-DUSE_EXTERNAL_JSON=ON"

do_install_append() {
    # Install configurations
    install -d ${D}${sysconfdir}/powertools
    install -m 0644 ${WORKDIR}/equip_testbench.json ${D}${sysconfdir}/powertools/equip_testbench.json
}

FILES_${PN} += "${sysconfdir}/powertools/equip_testbench.json \
	    ${datadir}/labRemote/schema/labremote_config_schema.json"

