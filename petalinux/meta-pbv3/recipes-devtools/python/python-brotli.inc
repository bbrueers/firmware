HOMEPAGE = "https://github.com/google/brotli"
SUMMARY = "Python bindings for the Brotli compression library"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=941ee9cd1609382f946352712a319b4b"

SRC_URI[md5sum] = "3444b534e86f65f9f229fd123965be87"
SRC_URI[sha256sum] = "0538dc1744fd17c314d2adc409ea7d1b779783b89fd95bcfb0c2acc93a6ea5a7"

BPN="Brotli"
PYPI_PACKAGE_EXT="zip"

inherit pypi
