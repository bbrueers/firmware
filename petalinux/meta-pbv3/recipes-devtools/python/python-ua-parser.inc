HOMEPAGE = "https://github.com/ua-parser/uap-python"
SUMMARY = "Python port of Browserscope's user agent parser"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://setup.py;md5=2c67ea3f3cc576b3de66df0178bbb080"

SRC_URI[md5sum] = "2a3c492e23794f644b2772d7198e32a7"
SRC_URI[sha256sum] = "47b1782ed130d890018d983fac37c2a80799d9e0b9c532e734c67cf70f185033"


inherit pypi
DEPENDS += " \
	${PYTHON_PN}-pyyaml-native \
"
