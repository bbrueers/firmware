HOMEPAGE = "https://github.com/maxcountryman/flask-seasurf/"
SUMMARY = "An updated CSRF extension for Flask."
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://setup.py;md5=90ce25e95196259d778a003a7d995cec"

SRC_URI[md5sum] = "7f5a522378f54bfacc25eb01fbe8916a"
SRC_URI[sha256sum] = "c57918c17e9afd988bdc30d8dcb7bfb833741dee38b06c1bbd17821d6fa2b6cf"
BPN="Flask-SeaSurf"


inherit pypi
RDEPENDS_${PN} += " \
    ${PYTHON_PN}-flask  \
"