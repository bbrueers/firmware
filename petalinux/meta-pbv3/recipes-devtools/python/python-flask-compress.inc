HOMEPAGE = "https://github.com/colour-science/flask-compress"
SUMMARY = "Compress responses in your Flask app with gzip or brotli."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=5a97e6c24800f56f6cecf15cc5af343f"

SRC_URI[md5sum] = "7fdafd5711c3b3d57cf924d7e13604c9"
SRC_URI[sha256sum] = "f367b2b46003dd62be34f7fb1379938032656dca56377a9bc90e7188e4289a7c"

BPN="Flask-Compress"

inherit pypi

RDEPENDS_${PN} += " \
    ${PYTHON_PN}-flask \
    ${PYTHON_PN}-brotli \
"