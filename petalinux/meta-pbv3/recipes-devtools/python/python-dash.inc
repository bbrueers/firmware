HOMEPAGE = "https://plotly.com/dash"
SUMMARY = "A Python framework for building reactive web-apps. Developed by Plotly."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=89692901c3f0cc5d8e0d7a6960fbb188"

inherit pypi
RDEPENDS_${PN} += " \
    ${PYTHON_PN}-flask  \
    ${PYTHON_PN}-flask-compress  \
    ${PYTHON_PN}-plotly  \
    ${PYTHON_PN}-dash-renderer  \
    ${PYTHON_PN}-dash-core-components  \
    ${PYTHON_PN}-dash-html-components  \
    ${PYTHON_PN}-dash-table  \
    ${PYTHON_PN}-future  \
"