HOMEPAGE = "https://github.com/tlsfuzzer/python-ecdsa"
SUMMARY = "Pure-Python ECDSA and ECDH"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=66ffc5e30f76cbb5358fe54b645e5a1d"

inherit pypi
