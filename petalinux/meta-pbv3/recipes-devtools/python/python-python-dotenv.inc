HOMEPAGE = "https://github.com/theskumar/python-dotenv"
SUMMARY = "Add .env support to your django/flask apps in development and deployments"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=55ee2c3471d386636a719c8ccac40b31"

SRC_URI[md5sum] = "80906b481060e266ebaa6748c192dc04"
SRC_URI[sha256sum] = "8c10c99a1b25d9a68058a1ad6f90381a62ba68230ca93966882a4dbc3bc9c33d"


inherit pypi
RDEPENDS_${PN} += " \
    ${PYTHON_PN}-typing \
"