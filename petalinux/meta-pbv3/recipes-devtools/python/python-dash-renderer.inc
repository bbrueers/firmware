HOMEPAGE = ""
SUMMARY = "Front-end component renderer for Dash"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://setup.py;md5=ff00f1283d66d69dfa102d058ee919be"

BPN="dash_renderer"

inherit pypi