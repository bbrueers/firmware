HOMEPAGE = "https://stuvel.eu/software/rsa/"
SUMMARY = "Pure-Python RSA implementation"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=c403f6882d4f97a9cd927df987d55634"

SRC_URI[md5sum] = "edb224f927cf8f53ff530ab04d092c69"
SRC_URI[sha256sum] = "5c6bd9dc7a543b7fe4304a631f8a8a3b674e2bbfc49c2ae96200cdbe55df6b17"

inherit pypi

DEPENDS += " \
        ${PYTHON_PN}-pytest-runner-native \
"

RDEPENDS_${PN} += " \
    ${PYTHON_PN}-pyasn1 \
"
