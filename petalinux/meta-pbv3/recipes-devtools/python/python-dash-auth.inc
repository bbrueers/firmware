HOMEPAGE = "https://plot.ly/dash"
SUMMARY = "Dash Authorization Package."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://setup.py;md5=1ac07f979b34288639df0823ffe9b5f1"

BPN="dash_auth"
inherit pypi

RDEPENDS_${PN} += " \
    ${PYTHON_PN}-flask \
    ${PYTHON_PN}-flask-compress \
    ${PYTHON_PN}-flask-seasurf \
    ${PYTHON_PN}-requests \
    ${PYTHON_PN}-plotly \
    ${PYTHON_PN}-chart-studio \
    ${PYTHON_PN}-dash \
    ${PYTHON_PN}-dash-html-components \
    ${PYTHON_PN}-dash-core-components \
    ${PYTHON_PN}-retrying \
    ${PYTHON_PN}-itsdangerous \
    ${PYTHON_PN}-ua-parser \
"
