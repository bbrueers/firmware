HOMEPAGE = ""
SUMMARY = "Core component suite for Dash"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://README.md;md5=178e147cacd2ca4bb62e3170f05a424b"

BPN="dash_core_components"

inherit pypi
