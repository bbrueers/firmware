#!/bin/bash

# Parse arguments
if [ ${#} -lt 2 ]; then
    echo "usage: ${0} petawork overlay0 [overlay1]"
    exit 1
fi
petawork=${1}
overlays=${@:2}

# Exit if any of these commands fails
set -e

POGDIR=$(realpath $(dirname "${BASH_SOURCE[0]}"))

# Create empty build directory for generated petalinux project
if [ -d ${petawork} ]; then
    rm -rf ${petawork}
fi

# Create petalinux project
petalinux-create -t project -n ${petawork} --template zynq 

#
# Add the requested overlays
for overlay in ${overlays}
do
    echo "Adding overlay ${overlay}"
    # Update configuration
    if [ -e ${overlay}/config ]; then
	${POGDIR}/ConfigPetalinux.sh ${petawork} ${overlay}/config
    fi

    if [ -e ${overlay}/rootfs_config ]; then
	${POGDIR}/ConfigPetalinux.sh ${petawork} ${overlay}/rootfs_config
    fi

    # Add exra layers
    for layer in $(find ${overlay} -type d -name 'meta-*')
    do
	# Add layer
	${POGDIR}/SetPetalinuxConfig.sh ${petawork}/project-spec/configs/config CONFIG_USER_LAYER_0 \"$(realpath ${layer})\"

	# Copy contents of rootfs menu
	if [ -e ${layer}/conf/user-rootfsconfig ]; then
	    echo "# ${layer}" >> ${petawork}/project-spec/meta-user/conf/user-rootfsconfig
	    cat ${layer}/conf/user-rootfsconfig >> ${petawork}/project-spec/meta-user/conf/user-rootfsconfig
	fi
    done
done
