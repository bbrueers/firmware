#!/bin/bash

# Parse arguments
if [ ${#} != 2 ]; then
    echo "usage: ${0} petawork name"
    exit 1
fi
petawork=${1}
name=${2}

# Exit if any of these commands fails
set -e

POGDIR=$(realpath $(dirname "${BASH_SOURCE[0]}"))

# Reference to work directory
workdir=$(pwd)

for i in $(find bin/ -name '*xsa')
do # Loop over all available hardware artifacts
    project=$(basename ${i} .xsa)
    pname=$(echo ${project} | cut -d- -f1)
    pver=$(echo ${project} | cut -d- -f2-)

    # Setup the version and firmware names
    ${POGDIR}/SetPetalinuxConfig.sh ${petawork}/project-spec/configs/config CONFIG_SUBSYSTEM_PRODUCT ${pname}
    ${POGDIR}/SetPetalinuxConfig.sh ${petawork}/project-spec/configs/config CONFIG_SUBSYSTEM_FW_VERSION ${pver}

    # Setup hardware for project
    petalinux-config -p ${petawork} --get-hw-description ${workdir}/bin/${project} --silentconfig

    # Build
    petalinux-build -p ${petawork}

    # Package
    petalinux-package -p ${petawork} --boot --fpga bin/${project}/${project}.bit --fsbl ${petawork}/images/linux/zynq_fsbl.elf --u-boot -o ${petawork}/images/linux/BOOT.bin --force

    # Save results
    target=bin/${project}/${name}
    if [ ! -e ${target} ]; then
	mkdir ${target}
    fi
    cp ${petawork}/images/linux/BOOT.bin ${target}
    cp ${petawork}/images/linux/image.ub ${target}
    cp ${petawork}/images/linux/boot.scr ${target}
    cp ${petawork}/images/linux/rootfs.tar.gz ${target}
done
