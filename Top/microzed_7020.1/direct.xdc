# I2C
set_property PACKAGE_PIN D19 [get_ports iic_rtl0_scl_io];
set_property PACKAGE_PIN D20 [get_ports iic_rtl0_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl0_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl0_sda_io]

# SPI
set_property PACKAGE_PIN D18 [get_ports {spi_rtl0_ss_io[0]}];
set_property PACKAGE_PIN E17 [get_ports {spi_rtl0_sck_io}];
set_property PACKAGE_PIN B20 [get_ports spi_rtl0_io1_io];
set_property PACKAGE_PIN C20 [get_ports spi_rtl0_io0_io];

set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl0_ss_io[0]}]

# AMAC
set_property PACKAGE_PIN N17 [get_ports CMD_IN_P_0_PB0]
set_property PACKAGE_PIN P18 [get_ports CMD_IN_N_0_PB0]
set_property PACKAGE_PIN V17 [get_ports CMD_OUT_P_0_PB0]
set_property PACKAGE_PIN V18 [get_ports CMD_OUT_N_0_PB0]

set_property PACKAGE_PIN R16 [get_ports CMD_IN_P_0_PB1]
set_property PACKAGE_PIN R17 [get_ports CMD_IN_N_0_PB1]
set_property PACKAGE_PIN Y18 [get_ports CMD_OUT_P_0_PB1]
set_property PACKAGE_PIN Y19 [get_ports CMD_OUT_N_0_PB1]

set_property PACKAGE_PIN T20 [get_ports CMD_IN_P_0_PB2]
set_property PACKAGE_PIN U20 [get_ports CMD_IN_N_0_PB2]
set_property PACKAGE_PIN N18 [get_ports CMD_OUT_P_0_PB2]
set_property PACKAGE_PIN P19 [get_ports CMD_OUT_N_0_PB2]

set_property PACKAGE_PIN U14 [get_ports CMD_IN_P_0_PB3]
set_property PACKAGE_PIN U15 [get_ports CMD_IN_N_0_PB3]
set_property PACKAGE_PIN T16 [get_ports CMD_OUT_P_0_PB3]
set_property PACKAGE_PIN U17 [get_ports CMD_OUT_N_0_PB3]

set_property PACKAGE_PIN Y16 [get_ports CMD_IN_P_0_PB4]
set_property PACKAGE_PIN Y17 [get_ports CMD_IN_N_0_PB4]
set_property PACKAGE_PIN T14 [get_ports CMD_OUT_P_0_PB4]
set_property PACKAGE_PIN T15 [get_ports CMD_OUT_N_0_PB4]

set_property PACKAGE_PIN F16 [get_ports CMD_IN_P_0_PB5]
set_property PACKAGE_PIN F17 [get_ports CMD_IN_N_0_PB5]
set_property PACKAGE_PIN M19 [get_ports CMD_OUT_P_0_PB5]
set_property PACKAGE_PIN M20 [get_ports CMD_OUT_N_0_PB5]

set_property PACKAGE_PIN K19 [get_ports CMD_IN_P_0_PB6]
set_property PACKAGE_PIN J19 [get_ports CMD_IN_N_0_PB6]
set_property PACKAGE_PIN K17 [get_ports CMD_OUT_P_0_PB6]
set_property PACKAGE_PIN K18 [get_ports CMD_OUT_N_0_PB6]

set_property PACKAGE_PIN J18 [get_ports CMD_IN_P_0_PB7]
set_property PACKAGE_PIN H18 [get_ports CMD_IN_N_0_PB7]
set_property PACKAGE_PIN F19 [get_ports CMD_OUT_P_0_PB7]
set_property PACKAGE_PIN F20 [get_ports CMD_OUT_N_0_PB7]

set_property PACKAGE_PIN J20 [get_ports CMD_IN_P_0_PB8]
set_property PACKAGE_PIN H20 [get_ports CMD_IN_N_0_PB8]
set_property PACKAGE_PIN H15 [get_ports CMD_OUT_P_0_PB8]
set_property PACKAGE_PIN G15 [get_ports CMD_OUT_N_0_PB8]

set_property PACKAGE_PIN L14 [get_ports CMD_IN_P_0_PB9]
set_property PACKAGE_PIN L15 [get_ports CMD_IN_N_0_PB9]
set_property PACKAGE_PIN K16 [get_ports CMD_OUT_P_0_PB9]
set_property PACKAGE_PIN J16 [get_ports CMD_OUT_N_0_PB9]

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_0_PB*]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_0_PB*]
set_property DIFF_TERM true [get_ports CMD_OUT_P_0_PB*]
