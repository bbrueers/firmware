# I2C
set_property PACKAGE_PIN L20 [get_ports iic_rtl1_scl_io];
set_property PACKAGE_PIN L19 [get_ports iic_rtl1_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl1_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl1_sda_io]

# SPI
set_property PACKAGE_PIN H17 [get_ports {spi_rtl1_ss_io[0]}];
set_property PACKAGE_PIN H16 [get_ports {spi_rtl1_sck_io}];
set_property PACKAGE_PIN G18 [get_ports spi_rtl1_io1_io];
set_property PACKAGE_PIN G17 [get_ports spi_rtl1_io0_io];

set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl1_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl1_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl1_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl1_ss_io[0]}]

# AMAC
set_property PACKAGE_PIN M17 [get_ports {CMD_OUT_P_1}];
set_property PACKAGE_PIN M18 [get_ports {CMD_OUT_N_1}];
set_property PACKAGE_PIN L16 [get_ports {CMD_IN_P_1}];
set_property PACKAGE_PIN L17 [get_ports {CMD_IN_N_1}];

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_1]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_1]
set_property DIFF_TERM true [get_ports CMD_OUT_N_1]
