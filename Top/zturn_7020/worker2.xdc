# I2C
set_property PACKAGE_PIN M17 [get_ports iic_rtl2_scl_io];
set_property PACKAGE_PIN M18 [get_ports iic_rtl2_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl2_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl2_sda_io]

# SPI
set_property PACKAGE_PIN B19 [get_ports {spi_rtl2_ss_io[0]}];
set_property PACKAGE_PIN A20 [get_ports {spi_rtl2_sck_io}];
set_property PACKAGE_PIN V7 [get_ports spi_rtl2_io0_io];
set_property PACKAGE_PIN U7 [get_ports spi_rtl2_io1_io];

set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl2_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl2_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl2_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl2_ss_io[0]}]

# AMAC
set_property PACKAGE_PIN D19 [get_ports {CMD_OUT_P_2}];
set_property PACKAGE_PIN D20 [get_ports {CMD_OUT_N_2}];
set_property PACKAGE_PIN F16 [get_ports {CMD_IN_P_2}];
set_property PACKAGE_PIN F17 [get_ports {CMD_IN_N_2}];

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_2]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_2]
set_property DIFF_TERM true [get_ports CMD_OUT_N_2]
