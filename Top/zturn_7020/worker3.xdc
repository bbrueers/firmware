# I2C
set_property PACKAGE_PIN U10 [get_ports iic_rtl3_scl_io];
set_property PACKAGE_PIN T9 [get_ports iic_rtl3_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl3_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl3_sda_io]

# SPI
set_property PACKAGE_PIN T15 [get_ports {spi_rtl3_ss_io[0]}];
set_property PACKAGE_PIN T14 [get_ports {spi_rtl3_sck_io}];
set_property PACKAGE_PIN T11 [get_ports spi_rtl3_io0_io];
set_property PACKAGE_PIN T10 [get_ports spi_rtl3_io1_io];

set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl3_ss_io[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl3_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl3_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl3_io0_io]

# AMAC
set_property PACKAGE_PIN U13 [get_ports {CMD_OUT_P_3}];
set_property PACKAGE_PIN V13 [get_ports {CMD_OUT_N_3}];
set_property PACKAGE_PIN Y7 [get_ports {CMD_IN_P_3}];
set_property PACKAGE_PIN Y6 [get_ports {CMD_IN_N_3}];

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_3]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_3]
set_property DIFF_TERM true [get_ports CMD_OUT_N_3]
