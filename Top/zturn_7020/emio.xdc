# I2C0
set_property PACKAGE_PIN P16 [get_ports I2C0_scl_io];
set_property PACKAGE_PIN P15 [get_ports I2C0_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports I2C0_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports I2C0_sda_io]
